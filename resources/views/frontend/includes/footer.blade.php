<div class="container-fluid">
    <div class="row">
        <div class="col-12 footer-logo-sec w-100 bg-secondary">
            <div class="footer-logo container px-5 py-3">
                <img src="{{asset('image/logo.png')}}"  class="bg-light px-4 py-3 rounded-3 " alt="Footer logo">
            </div>
        </div>
        <div class="col-12 footer-link-sec bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="row ">
                            <div class="col-12 col-lg-3 text-light py-3 footer-link">
                                <ul class="list-unstyled ">
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light text-light">আজকের পত্রিকা</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">জাতীয়</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">আন্তর্জাতিক</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">রাজনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">অর্থনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">সারাদেশ</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">ইসলাম ও জীবন</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">একদিন প্রতিদিন</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-3 text-light py-3 footer-link">
                                <ul class="list-unstyled ">
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light text-light">আজকের পত্রিকা</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">জাতীয়</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">আন্তর্জাতিক</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">রাজনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">অর্থনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">সারাদেশ</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">ইসলাম ও জীবন</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">একদিন প্রতিদিন</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-3 text-light py-3 footer-link">
                                <ul class="list-unstyled ">
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light text-light">আজকের পত্রিকা</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">জাতীয়</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">আন্তর্জাতিক</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">রাজনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">অর্থনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">সারাদেশ</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">ইসলাম ও জীবন</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">একদিন প্রতিদিন</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-3 text-light py-3 footer-link">
                                <ul class="list-unstyled ">
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light text-light">আজকের পত্রিকা</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">জাতীয়</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">আন্তর্জাতিক</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">রাজনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">অর্থনীতি</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">সারাদেশ</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">ইসলাম ও জীবন</a></li>
                                    <li class="my-2"><a href="#" class="text-decoration-none text-light">একদিন প্রতিদিন</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="row pt-5">
                            <div class="col-12 my-2">
                                <a href="#"> <img src="{{asset('image/google-play.png')}}" alt="playstore"></a>
                            </div>
                            <div class="col-12 my-2">
                                <a href="#"><img src="{{asset('image/converter.png')}}" alt="converter"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-6 col-md-6  bg-secondary text-center">
            <p class=" text-light">এই ওয়েবসাইটের কোনো লেখা, ছবি, অডিও, ভিডিও অনুমতি ছাড়া ব্যবহার বেআইনি।</p>
        </div>
        <div class="col-12 col-lg-6 col-md-6 bg-secondary text-center">
            <p class=" text-light">Copyright &copy; All Reserve 2021</p>
        </div>
    </div>
</div>
