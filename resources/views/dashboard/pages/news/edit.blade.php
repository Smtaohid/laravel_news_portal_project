
@extends('dashboard.layout.master')
@section('page_title','News Edit')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-10 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center">Edit News</h3></div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif
                <div class="card-body">

                    {!! Form::model($news,['route'=>['news.update', $news->id], 'method'=>'put', 'files'=>true]) !!}
                    @include('dashboard.pages.news.form')
                    @if($news->photo != null)

                      <div class="news-img">
                          <span class="mt-5">Previous Image</span> <br>
                          <img src="{{url('image/uploads/news').'/'.$news->photo}}" alt="{{$news->title}}">
                      </div>
                    @endif
                    {!! Form::button('<i class="fas fa-plus mx-1"></i>Update',['type'=>'submit', 'class'=>'btn btn-success ps-2 mt-3 form-control']) !!}
                    {!! Form::close() !!}
                    <a href="{{route('news.index')}}" class="mt-3 form-control btn btn-warning ">Back</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js" integrity="sha512-u9akINsQsAkG9xjc1cnGF4zw5TFDwkxuc9vUp5dltDWYCSmyd0meygbvgXrlc/z7/o4a19Fb5V0OUE58J7dcyw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>

            const get_sub_categories =(category_id, sub_category_id)=>{
                axios.get(window.location.origin+'/dashboard/get-sub-categories/'+category_id).then(res=>{
                    let element = $('#sub_category_id')
                    element.empty()
                    let  sub_category = res.data
                    for(let i=0; i < Object.keys(sub_category).length; i++){
                        let selected = sub_category_id == sub_category[i].id ?'selected':''
                        element.append('<option '+selected+' value="'+sub_category[i].id+'">'+sub_category[i].name+'</option>')
                    }
                })
            }
            get_sub_categories('<?php echo $news->category_id?>','<?php echo $news->sub_category_id?>')
            $('#category_id').on('change',function () {
                let category_id = $(this).val()
                get_sub_categories(category_id)
            })
    </script>



    <script>
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
