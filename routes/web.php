<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Route::get('/',[FrontendController::class, 'index'])->name('front.index');
Route::get('/single-news',[FrontendController::class, 'single_news'])->name('front.single_news');
//Route::get('/add-slug',[FrontendController::class, 'add_slug']);
Route::group(['prefix'=> 'dashboard','middleware'=>'auth'],function(){

   Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
   Route::get('get-sub-categories/{id}',[SubCategoryController::class,'get_sub_categories']);
    /*
   Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');
   Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
   Route::post('/categories',[CategoryController::class,'store'])->name('categories.store');
   Route::get('/categories/{category}',[CategoryController::class,'show'])->name('categories.show');
   Route::get('/categories/{category}/edit',[CategoryController::class,'edit'])->name('categories.edit');
   Route::put('/categories/{category}',[CategoryController::class,'update'])->name('categories.update');
   Route::delete('/categories/{category}',[CategoryController::class,'destroy'])->name('categories.destroy');
*/
   Route::resource('categories',CategoryController::class);
   Route::resource('sub-categories', SubCategoryController::class);
   Route::resource('news', NewsController::class);
});
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/{slug}',[FrontendController::class, 'category'])->name('front.category');
