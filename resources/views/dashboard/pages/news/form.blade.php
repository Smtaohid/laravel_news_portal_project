{!! Form::label('title','Title') !!}
{!! Form::text('title',null, ['class'=>'form-control','placeholder'=>'Insert News Title'])!!}
{!! Form::label('description','Description',['class'=>'mt-3']) !!}
{!! Form::textarea('description',null, ['class'=>'form-control','placeholder'=>'Insert News Details','id'=>'description'])!!}
<div class="row">
    <div class="col-12 col-lg-6">
        {!! Form::label('category_id','Select Category',['class'=>'mt-3']) !!}
        {!! Form::select('category_id',$categories,null, ['class'=>'form-select','placeholder'=>'Select Category','id'=>'category_id'])!!}
    </div>
    <div class="col-12 col-lg-6">
        {!! Form::label('sub_category_id','Select Sub Category',['class'=>'mt-3']) !!}
        {!! Form::select('sub_category_id',$subCategories,null, ['class'=>'form-select','placeholder'=>'Select Sub Category','id'=>'sub_category_id'])!!}
    </div>
</div>
<div class="row">
    <div class="col-12 col-lg-6">
        {!! Form::label('photo','Photo',['class'=>'mt-3 d-block']) !!}
        {!! Form::file('photo', ['class'=>'form-control','placeholder'=>'Insert Photo'])!!}
    </div>
    <div class="col-12 col-lg-6">
        {!! Form::label('status','Select Status',['class'=>'mt-3']) !!}
        {!! Form::select('status',['1'=>'Publish','2'=>'Unpublished'],null, ['class'=>'form-select','placeholder'=>'Insert News Details'])!!}
    </div>
</div>
