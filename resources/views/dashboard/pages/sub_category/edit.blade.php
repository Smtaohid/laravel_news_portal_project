

@extends('dashboard.layout.master')
@section('page_title','Sub Category Edit')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-10 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center">Edit Sub Category</h3></div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif
                <div class="card-body">

                    {!! Form::model($subCategory,[ 'method'=>'put','route'=>['sub-categories.update',$subCategory->id]]) !!}
                    @include('dashboard.pages.sub_category.form')
                    {!! Form::button('<i class="fas fa-plus mx-1"></i>Update',['type'=>'submit', 'class'=>'btn btn-success ps-2 mt-3 form-control']) !!}
                    {!! Form::close() !!}
                    <a href="{{route('sub-categories.index')}}" class="mt-3 form-control btn btn-warning ">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
