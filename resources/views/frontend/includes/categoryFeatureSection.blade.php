@isset($feature_news)
<div class="row mb-5">
        <div class="col-lg-8">
            <div class="feature-news">
                <a href="">
                    <img src="{{asset('image/uploads/news/').'/'.$feature_news->photo}}" class="img-fluid" alt="Feature Image" />
                    <div class="content">
                        <h2>{{$feature_news->title}}</h2>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            @foreach($beside_feature_news as $news)
            <div class="feature-news {{$loop->index == 1 ? 'mt-3':null}} ">
                <a href="">
                    <img src="{{asset('image/uploads/news/').'/'.$news->photo}}" class="img-fluid" alt="Feature Image" />
                    <div class="content">
                        <h5>{{$news->title}}</h5>
                    </div>
                </a>
            </div>
            @endforeach
{{--            <div class="feature-news mt-3">--}}
{{--                <a href="">--}}
{{--                    <img src="{{asset('image/news/5.jpg')}}" class="img-fluid" alt="Feature Image" />--}}
{{--                    <div class="content">--}}
{{--                        <h5>বঙ্গবন্ধুর হত্যাকারীদের দূতাবাসে চাকরি দেয় জিয়া, খালেদা বানিয়েছেন এমপি</h5>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}

        </div>
    </div>

@endisset
