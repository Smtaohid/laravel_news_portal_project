<!DOCTYPE html>
<html lang="en">
<head>
   @include('dashboard.includs.head')
</head>
<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    @include('dashboard.includs.nav')
</nav>
<div id="layoutSidenav">
    @include('dashboard.includs.sitenav')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">@yield('page_title')</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">@yield('page_title')</li>
                </ol>
                     @yield('content')
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
          @include('dashboard.includs.footer')
        </footer>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset('admin/js/scripts.js')}}"></script>
</body>
</html>
