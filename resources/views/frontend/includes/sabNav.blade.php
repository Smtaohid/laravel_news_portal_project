<div class="row">
        <div class="col-lg-12 category-page-sub-menu-container">
            <div class="row">
                <div class="col-lg-1">
                    <h4>{{$category->name}}</h4>
                </div>
                <div class="col-lg-11">
                    <div class="category-page-sub-menu">
                        @foreach($sub_categories as $sub_category)
                        <a href="">{{$sub_category->name}}</a>
{{--                        <a href="">অপরাধ</a>--}}
{{--                        <a href="">আইন-বিচার</a>--}}
{{--                        <a href="">গণমাধ্যম</a>--}}
{{--                        <a href="">দুর্ঘটনা</a>--}}
{{--                        <a href="">শোক</a>--}}
{{--                        <a href="">অন্যান্য</a>--}}
{{--                        <a href="">জাতীয় সব খবর</a>--}}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

