
@extends('dashboard.layout.master')
@section('page_title','Category Details')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-7 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"> Sub Category Details</h3>
                </div>
                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                        <tr>
                            <th>Sub Name</th>
                            <td>{{$subCategory->name}}</td>
                        </tr><tr>
                            <th>Category Name</th>
                            <td>{{$subCategory->category->name}}</td>
                        </tr>
                        <tr>
                            <th>Order By</th>
                            <td>{{$subCategory->order_by}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>{!! $subCategory->status == 1 ? '<span class="text-success">Active</span>': '<span class="text-danger">Inactive</span>' !!}</td>
                        </tr>
                        <tr>
                            <th>Crated At</th>
                            <td>{{$subCategory->created_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr>
                            <th>Updated At</th>
                            <td>{{$subCategory->updated_at == $subCategory->created_at ?'Not Updated Yet': $subCategory->updated_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr>
                            <th>Create By</th>
                            <td>{{$subCategory->user->name}}</td>
                        </tr>

                    </table>
                    <div class="card-link g-5">
                        <a href="{{route('sub-categories.index')}}" class="btn btn-success">Back</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
