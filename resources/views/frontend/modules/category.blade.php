@extends('frontend.layouts.categoryMaster')
@section('content')
    <div class="row">
        <!--Main body content section start-->

        @foreach($all_news->sub_category as $sub_category)
    <div class="col-lg-12 {{$loop->index != 0 ? 'mt-4':null}}">

            <h3 class="category-page-news-area-headline">{{$sub_category->name}}</h3>
            <div class="row">
                @foreach($sub_category->news as $news)
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/uploads/news/').'/'.$news->photo}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>{{$news->title}}</h5>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @endforeach


</div>
    <!--Main body content section end-->

@endsection
