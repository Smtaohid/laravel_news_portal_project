
<!--Menu section start-->
<section>
    <div class="container menu-container position-relative">
        <div class="row" id="menu_container">
            <div class="col-lg-3 ps-0">
                <img src="{{asset('image/logo.png')}}" class="logo" alt="logo" />
            </div>
            <div class="col-lg-7">
                <div class="menu" id="menu">
                    @foreach($main_menu as $menu)
                    <a href="{{route('front.category',$menu->slug)}}">{{$menu->name}}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-2">
                <div class="right-menu-items">
                    <div class="more-menu" id="more_menu">
                        <i class="fas fa-bars"></i> আরও
                    </div>
                    <div class="search" id="search_toggle">
                        <i class="fas fa-search"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="search_area" style="display: none">
            <div class="col-lg-12 gx-0 py-2">
                <div class="input-group">
                    <input class="form-control">
                    <span id="search" class="input-group-text bg-info text-white border-info"><i
                            class="fas fa-search"></i></span>
                    <span id="search_close" class="input-group-text bg-danger text-white border-danger"><i
                            class="fas fa-times"></i></span>
                </div>
            </div>
        </div>
        <div class="row mega-menu" id="mega_menu" style="display: none">
            <div class="col-lg-12">
                <div class="row py-3">
                    @foreach($all_menu as $menu)
                    <div class="col-lg-2">
                        <a href="{{route('front.category',$menu->slug)}}">{{$menu->name}}</a>

                    </div>
                    @endforeach
                </div>
                <div class="row mega-menu-bottom">
                    <div class="col-lg-2">
                        <a href=""><i class="fas fa-file-alt"></i> আজকের পত্রিকা</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="fas fa-file-alt text-success"></i> দ্বিতীয় সংস্করণ </a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="far fa-newspaper text-info"></i> ই-পেপার</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="far fa-image"></i> ছবি</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="fas fa-file-video"></i> ভিডিও</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="fas fa-archive text-danger"></i> আর্কাইভ</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="fab fa-android text-success"></i> অ্যান্ড্রয়েড</a>
                    </div>
                    <div class="col-lg-2">
                        <a href=""><i class="fas fa-briefcase"></i> বিজ্ঞাপন</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Menu section end-->
