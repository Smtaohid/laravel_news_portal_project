<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $subCategories = SubCategory::orderBy('order_by', 'asc')->with('category', 'user')->paginate(30);
        return \view('dashboard.pages.sub_category.index', compact('subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('dashboard.pages.sub_category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'status' => 'required',
            'order_by' => 'required',
        ]);
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        SubCategory::create($data);
        session()->flash('msg', 'Sub Category Create Successfully');
        return redirect()->route('sub-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param SubCategory $subCategory
     * @return Application|Factory|View
     */
    public function show(SubCategory $subCategory)
    {
        $subCategory->load('category', 'user');
        return view('dashboard.pages.sub_category.show', compact('subCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SubCategory $subCategory
     * @return Application|Factory|View
     */
    public function edit(SubCategory $subCategory)
    {
        $categories = Category::pluck('name', 'id');
        return \view('dashboard.pages.sub_category.edit', compact('subCategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param SubCategory $subCategory
     * @return RedirectResponse
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        $request->validate([
            'name' => 'required',
            'order_by' => 'required',
            'status' => 'required']);
        session()->flash('msg', 'Category Updated Successfully');
        $subCategory->update($request->all());
        return redirect()->route('sub-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SubCategory $subCategory
     * @return RedirectResponse
     */
    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();
        session()->flash('msg', 'Sub Category Deleted Successfully');
        return redirect()->route('sub-categories.index');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function get_sub_categories($id): JsonResponse
    {
       $subCategories = SubCategory::select('name','id')->where('category_id',$id)->get();
       return response()->json($subCategories);
    }
}
