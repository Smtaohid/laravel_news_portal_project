@extends('dashboard.layout.master')
@section('page_title','subCategory List')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center"> subCategory list</h3></div>

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif

                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                        <thead>
                        <tr class="text-center">
                            <th>Sl</th>
                            <th>Sub Category Name</th>
                            <th>Category Name</th>
                            <th>Order By</th>
                            <th>Stats</th>
                            <th>Create By</th>
                            <th>Create At</th>
                            <th>Update At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $sl = 1;
                        @endphp
                        @foreach($subCategories as $subCategory)

                            <tr>
                                <th>{{$sl++}}</th>
                                <th>{{$subCategory->name}}</th>
                                <th>{{$subCategory->category->name}}</th>
                                <th>{{$subCategory->order_by}}</th>
                                <th>{!!$subCategory->status == 1 ? '<span class="text-success">Active</span>':'<span class="text-danger">Inactive</span>'!!}</th>
                                <th>{{$subCategory->user->name}}</th>
                                <th>{{$subCategory->created_at->toDayDateTimeString()}}</th>
                                <th>{{$subCategory->updated_at == $subCategory->created_at ? 'Not Updated':$subCategory->updated_at->toDayDateTimeString()}}</th>
                                <th class="d-flex justify-content-between">
                                    <a href="{{route('sub-categories.show', $subCategory->id)}}"> <button class="btn btn-success btn-sm mx-2 text-white"><i class="fas fa-eye"></i></button></a>
                                    <a href="{{route('sub-categories.edit',$subCategory->id)}}"><button class="btn btn-warning btn-sm mx-2"><i class="fas fa-edit"></i></button></a>
                                    {!! Form::open(['method'=>'delete','route'=>['sub-categories.destroy',$subCategory->id]]) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i>',['type'=>'submit', 'class'=>'btn btn-danger btn-sm mx-2','onclick'=>'return confirm("Age you Sure")']) !!}
                                    {!! Form::close() !!}
                                </th>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                    {{ $subCategories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
