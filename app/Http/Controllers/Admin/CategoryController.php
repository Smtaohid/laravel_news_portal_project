<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories  = Category::orderBy('order_by', 'asc')->with('user')->paginate(20);
    return view('dashboard.pages.category.index', compact('categories'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('dashboard.pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     */
    public function store(Request $request)
    {
//        return 'hello';
        $request->validate([
            'name' => 'required',
            'order_by'=> 'required',
            'status' => 'required']);
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
       Category::create($data);
        session()->flash('msg', 'Category Inserted Successfully');
       return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View
     */
    public function show(Category $category)
    {
        /*try {
           $category = Category::with('user')->findOrFail($id);
       }catch (ModelNotFoundException $e){
           $category = $e->getMessage();
       }*/
//      $category = Category::with('user')->findOrFail($id);

        return \view('dashboard.pages.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View|Response
     */
    public function edit(Category $category)
    {
        return \view('dashboard.pages.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            'order_by'=> 'required',
            'status' => 'required']);
        session()->flash('msg', 'Category Updated Successfully');
        $category->update($request->all());
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return RedirectResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash('msg', 'Data  Deleted Successfully');
        return redirect()->back();
    }

    private function validate(Request $request, array $array)
    {
    }
}
