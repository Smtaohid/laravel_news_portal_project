
@extends('dashboard.layout.master')
@section('page_title','Category Details')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-7 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"> Category Details</h3>
                </div>
                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                        <tr>
                            <th>Name</th>
                            <td>{{$category->name}}</td>
                        </tr>
                        <tr>
                            <th>Order By</th>
                            <td>{{$category->order_by}}</td>
                        </tr>
                        <tr>
                             <th>Status</th>
                             <td>{!! $category->status == 1 ? '<span class="text-success">Active</span>': '<span class="text-danger">Inactive</span>' !!}</td>
                        </tr>
                        <tr>
                             <th>Crated At</th>
                             <td>{{$category->created_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr>
                             <th>Updated At</th>
                             <td>{{$category->updated_at == $category->created_at ?'Not Updated Yet': $category->updated_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr>
                             <th>Create By</th>
                             <td>{{$category->user->name}}</td>
                        </tr>

                    </table>
                    <div class="card-link g-5">
                        <a href="{{route('categories.index')}}"class="btn btn-success">Back</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
