@extends('dashboard.layout.master')
@section('page_title','News List')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center"> News list</h3></div>

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif

                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                        <thead>
                        <tr class="text-center">
                            <th>Sl</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Photo</th>
                            <th>Create By</th>
                            <th>Create At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $sl = 1 ;@endphp
                        @foreach($newes as $news)
                            <tr class="news-list">
                                <th>{{$sl++}}</th>
                                <td class="news-content">{!! substr($news->title, 0 ,50) ,'...' !!}</td>
                                <td class="news-desc">{!! substr($news->description, 0 ,100),'...' !!}</td>
                                <td class="news-desc"><span class="fw-bold news-desc">{{$news->category != null? $news->category->name:null}}</span> <br>
                                <small class="text-primary ps-3">{{$news->sub_category != null? $news->sub_category->name:null}}</small>
                                </td>
                                <td class="news-desc fw-bold">{{$news->status == 1? 'Published':'Unpublished'}}</td>
                                <td class="news-desc">
                                    @if($news->photo != null)
                                        <img class="img-thumbnail" width="100px" src="{!! url('image/uploads/news').'/'.$news->photo !!}" alt="{{$news->title}}">
                                    @endif

                                </td>
                                <td class="news-desc fw-bold">{{$news->user != null ? $news->user->name:null}}</td>
                                <td class="news-desc fw-bold">{{$news->created_at}}</td>
                                <th class="d-flex justify-content-between">
                                    <a class="mx-2" href="{{route('news.show', $news->id)}}"> <button class="btn btn-success btn-sm text-white"><i class="fas fa-eye"></i></button></a>
                                    <a class="mx-2" href="{{route('news.edit',$news->id)}}"><button class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button></a>
                                    {!! Form::open(['method'=>'delete','route'=>['news.destroy',$news->id]]) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i>',['type'=>'submit', 'class'=>'btn btn-danger btn-sm ','onclick'=>'return confirm("Age you Sure")']) !!}
                                    {!! Form::close() !!}
                                </th>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $newes->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
