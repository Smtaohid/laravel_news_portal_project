@extends('dashboard.layout.master')
@section('page_title','News Create')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center">Create News</h3></div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif
                <div class="card-body">
                    {!! Form::open(['route'=>'news.store','method'=>'post','files'=>'true']) !!}

                    @include('dashboard.pages.news.form')

                    {!! Form::button('Save News',['class'=>'form-control btn btn-success mt-3','type'=>'submit'])!!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js" integrity="sha512-u9akINsQsAkG9xjc1cnGF4zw5TFDwkxuc9vUp5dltDWYCSmyd0meygbvgXrlc/z7/o4a19Fb5V0OUE58J7dcyw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $('#category_id').on('change',function () {
            let category_id = $(this).val()
            axios.get(window.location.origin+'/dashboard/get-sub-categories/'+category_id).then(res=>{
               let element = $('#sub_category_id')
               element.empty()
                let  sub_category = res.data
                for(let i=0; i < Object.keys(sub_category).length; i++){
                  element.append('<option value="'+sub_category[i].id+'">'+sub_category[i].name+'</option>')
               }
            })
        })
    </script>



    <script>
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
