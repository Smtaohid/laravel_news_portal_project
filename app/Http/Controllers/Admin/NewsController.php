<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\Models\SubCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Commands\AbstractCommand;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $newes = News::latest()->with('user')->with('category')->with('sub_category')->paginate(20);

        return \view('dashboard.pages.news.index',compact('newes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::pluck('name','id');
        $subCategories = SubCategory::pluck('name','id');
        return view('dashboard.pages.news.create',compact('categories','subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
          'title'=> 'required|max:191',
          'description'=> 'required',
          'category_id'=> 'required',
          'status'=> 'required',
        ]);
        $news = $request->all();
        $news['user_id'] = Auth::user()->id;
        if ($request->hasFile('photo')){
            $name = Str::of($request->title)->slug().'.webp';
            $news['photo'] =$name;
            Image::make($request->file('photo'))->save(public_path('image/uploads/news').'/'.$name);
        }else{
            $news['photo'] = null;
        }
        session()->flash('msg','News Created Successfully');
        News::create($news);
        return redirect()->route('news.index');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $news = News::with('user','category','sub_category')->findOrFail($id);
        return \view('dashboard.pages.news.show',compact('news'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param News $news
     * @return Application|Factory|View
     */
    public function edit(News  $news)
    {
        $categories = Category::pluck('name','id');
        $subCategories = SubCategory::pluck('name','id');
        return \view('dashboard.pages.news.edit',compact('news','categories','subCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param News $news
     * @return RedirectResponse
     */
    public function update(Request $request, News $news): RedirectResponse
    {

        $request->validate([
            'title'=> 'required|max:191',
            'description'=> 'required',
            'category_id'=> 'required',
            'status'=> 'required',
        ]);
        $newNews = $request->all();
        $newNews['user_id'] = Auth::user()->id;
        if ($request->hasFile('photo')){
            $name = Str::of($request->title)->slug().'.webp';
            $newNews['photo'] =$name;
            Image::make($request->file('photo'))->save(public_path('image/uploads/news').'/'.$name);
        }else{
            $newNews['photo'] = $news->photo;
        }
        $news->update($newNews);
        session()->flash('msg','News Updated Successfully');

        return redirect()->route('news.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return RedirectResponse
     */
    public function destroy(News $news): RedirectResponse
    {
        if ($news->photo!= null){
            $path = public_path('image/uploads/news').'/'.$news->photo;
            unlink($path);
        }
        $news->delete();
        session()->flash('msg','News Delete Successfully');
        return redirect()->back();
    }
}
