<div class="col-lg-4">
    <div class="sidebar-button-area">
        <button><i class="fas fa-chart-line"></i> শেয়ার বাজার</button>
        <button><i class="fas fa-book-open"></i> টিউটোরিয়াল</button>
        <button><i class="fas fa-photo-video"></i> চিত্র বিচিত্র</button>
        <button><i class="fas fa-mosque"></i> ইসলাম ও জীবন</button>
    </div>
    <div class="news-feed-area mt-4">
        <div class="news-feed-nav">
            <button id="latest_news_button" class="active">সর্বশেষ</button>
            <button id="most_read_news_button">সর্বাধিক পঠিত</button>
        </div>
        <div id="latest_news" class="news-feed-latest mt-4">
            @foreach($latest_news as $news)
            <div class="row mt-2">
                <div class="col-3">
                    <img src="{{asset('image/uploads/news/').'/'.$news->photo}}" class="img-fluid" alt="news"/>
                </div>
                <div class="col-9">
                    <a href="">{{$news->title}}</a>
                </div>
            </div>
            @endforeach
        </div>
        <div id="most_read_news" style="display: none" class="news-feed-latest mt-4">

            @foreach($most_read_news as $news)
            <div class="row mt-2">
                <div class="col-3">
                    <img src="{{asset('image/uploads/news/').'/'.$news->photo}}" class="img-fluid" alt="news"/>
                </div>
                <div class="col-9">
                    <a href="">{{$news->title}}</a>
                </div>
            </div>
            @endforeach

        </div>
        <div class="news-feed-all-news-button">
            <button>সব খবর</button>
        </div>
    </div>
    <div class="survey-area my-5">
        <div class="card">
            <div class="card-header">
                <h4>অনলাইন জরিপ</h4>
            </div>
            <div class="card-body">
                <p>স্বাস্থ্যমন্ত্রী জাহিদ মালেক বলেছেন, আগামী বছরের ফেব্রুয়ারির মধ্যে দেশে আরও ৭ থেকে ৮ কোটি
                    মানুষকে টিকা দেওয়া হবে। এটি সম্ভব হবে বলে মনে করেন কি?</p>

                <div class="survey-input-group-container">
                    <div class="survey-input-group">
                        <input type="radio" id="test1" name="radio-group" checked>
                        <label for="test1"></label> হ্যাঁ
                    </div>
                    <div class="survey-input-group">
                        <input type="radio" id="test2" name="radio-group" checked>
                        <label id="test_label_2" for="test2"></label> না
                    </div>
                    <div class="survey-input-group">
                        <input type="radio" id="test3" name="radio-group" checked>
                        <label id="test_label_3" for="test3"></label> মন্তব্য নেই
                    </div>
                </div>
                <div class="my-4 text-center">
                    <button class="btn btn-success">মতামত দিন</button>
                </div>
                <div class="survey-result">
                    <a href="">পুরনো ফলাফল ></a>
                </div>
            </div>
        </div>
    </div>
    <div class="advertisement-area my-5">
        <div class="card">
            <div class="card-header">
                <h4>বিজ্ঞাপন</h4>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
</div>
