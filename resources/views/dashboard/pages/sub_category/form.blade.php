{!! Form::label('name','Sub Category Name',['class'=>'lead ps-2']) !!}
{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Insert Sub Category name']) !!}
{!! Form::label('category_id','Select Category',['class'=>'lead ps-2']) !!}
{!! Form::select('category_id',$categories,null,['class'=> 'form-control','placeholder'=>'Select Category']) !!}

{!! Form::label('order_by','Order By',['class'=>'lead ps-2']) !!}
{!! Form::number('order_by',null, ['class'=>'form-control','placeholder'=>'Insert Menu Order']) !!}
{!! Form::label('status','Status',['class'=>'lead ps-2']) !!}
{!! Form::select('status',['1'=>'Active','2'=>'Inactive'],null,['class'=>'form-control','placeholder'=>'Select Option']) !!}
