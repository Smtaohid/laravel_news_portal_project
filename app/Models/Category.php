<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static pluck(string $string, string $string1)
 * @method static orderBy(string $string, string $string1)
 * @method static where(string $string, string $slug)
 */
class Category extends Model
{
    use HasFactory;

//   protected $fillable=['name','status','order_by', 'user_id','slug'];
protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(user::class);
    }
    public function sub_category(): HasMany
    {
        return $this->hasMany(SubCategory::class);
    }
}
