
@extends('dashboard.layout.master')
@section('page_title','News Details')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-10 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"> News Details</h3>
                </div>
                <div class="card-img show-news-img">
                    @if($news->photo != null)
                        <img src="{{url('image/uploads/news'.'/'.$news->photo)}}" alt="{{$news->title}}">
                    @endif
                </div>
                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                        <tr class="news-list">
                            <th>Title</th>
                            <td><h4 class="text-primary fw-bold">{{$news->title}}</h4></td>
                        </tr>
                        <tr class="news-list">
                            <th>Description</th>
                            <td ><div class="show-news-desc">{!! $news->description !!}</div></td>
                        </tr>
                        <tr class="news-list">
                            <th>Category</th>
                            <td >
                                <h6 class="fw-bold">{{$news->category->name}}</h6>
                                <span class="text-primary ps-5">{{$news->sub_category->name}}</span>
                            </td>
                        </tr>
                        <tr class="news-list">
                            <th>Status</th>
                            <td>{!! $news->status == 1 ? '<span class="text-success">Published</span>': '<span class="text-danger">Unpublished</span>' !!}</td>
                        </tr>
                        <tr class="news-list">
                            <th>Crated At</th>
                            <td>{{$news->created_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr class="news-list">
                            <th>Updated At</th>
                            <td>{{$news->updated_at == $news->created_at ?'Not Updated Yet': $news->updated_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr class="news-list">
                            <th>Create By</th>
                            <td>{{$news->user->name}}</td>
                        </tr>

                    </table>
                    <div class="card-link g-5">
                        <a href="{{route('news.index')}}" class="btn btn-success">Back</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
