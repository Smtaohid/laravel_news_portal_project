<?php

namespace Database\Factories;
use App\Models\News;
use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = News::class;

    /**
     * @return array
     */
    public function definition(): array
    {
        return [
             'category_id' => $this->faker->numberBetween(116, 125),

             'description'=> $this->faker->paragraph(),
             'photo'=> $this->faker->image('public/image/uploads/news',728,410,'news',false),
             'status'=> 1,
             'title'=>$this->faker->sentence(),
             'user_id'=>1,
            'sub_category_id'=>null


        ];
    }

}
