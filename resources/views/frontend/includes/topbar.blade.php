<!--top bar section start-->
<section>
    <div class="container top-bar-container">
        <div class="row">
            <div class="col-lg-9">
                <div class="top-bar-link text-center text-lg-start ps-0 ps-lg-3">
                    <ul>
                        <li>ঢাকা, বাংলাদেশ</li>
                        <li>মঙ্গলবার, ১৭ আগস্ট ২০২১, ২ ভাদ্র ১৪২৮</li>
                        <li><a href="">আজকের পত্রিকা</a></li>
                        <li><a href="">ই-পেপার</a></li>
                        <li><a href="">আর্কাইভ</a></li>
                        <li><a href="">কনভার্টার</a></li>
                        <li><a href="">অ্যাপস</a></li>
                        <li>বেটা ভার্সন</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="top-bar-social-icon text-center text-lg-end pe-0 pe-lg-3">
                    <a href=""><i class="fab fa-facebook-square"></i></a>
                    <a href=""><i class="fab fa-instagram-square"></i></a>
                    <a href=""><i class="fab fa-twitter-square"></i></a>
                    <a href=""><i class="fab fa-linkedin"></i></a>
                    <a href=""><i class="fab fa-pinterest-square"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--top bar section end-->
