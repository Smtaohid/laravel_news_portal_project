<?php

namespace App\Http\Controllers\Frontend;



use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\Models\SubCategory;
use Illuminate\Support\Str;


class FrontendController extends Controller
{

    public function index()
    {
        $cover_feature = News::latest()->first();
//        $beside_cover_feature = News::orderBy('id','desc')->skip(1)->take(2)->get();
//        $bellow_cover_feature = News::orderBy('id','desc')->skip(3)->take(9)->get();
        $beside_cover_feature = News::latest()->skip(1)->take(2)->get();
        $bellow_cover_feature = News::latest()->skip(3)->take(9)->get();

        $national_feature   = News::orderBy('id','desc')->where('category_id',2)->first();
        $national_news = News::orderBy('id','desc')->where('category_id',2)->skip(1)->take(6)->get();

        $politics_feature   = News::orderBy('id','desc')->where('category_id',3)->first();
        $politics_news = News::orderBy('id','desc')->where('category_id',3)->skip(1)->take(6)->get();

        $internatial_feature = News::orderBy('id','desc')->where('category_id',5)->first();
        $internatial_news = News::orderBy('id','desc')->where('category_id',5)->skip(1)->take(6)->get();

        $whole_country_feature  = News::orderBy('id','desc')->where('category_id',6)->first();
        $whole_country_news = News::orderBy('id','desc')->where('category_id',6)->skip(1)->take(6)->get();

        $sports_feature   = News::orderBy('id','desc')->where('category_id',7)->first();
        $sports_news = News::orderBy('id','desc')->where('category_id',7)->skip(1)->take(4)->get();

        $it_world_feature   = News::orderBy('id','desc')->where('category_id',26)->first();
        $it_world_news = News::orderBy('id','desc')->where('category_id',26)->skip(1)->take(6)->get();

        $economics_feature   = News::orderBy('id','desc')->where('category_id',4)->first();
        $economics_news = News::orderBy('id','desc')->where('category_id',4)->skip(1)->take(6)->get();

        $capital_feature   = News::orderBy('id','desc')->where('category_id',17)->first();
        $capital_news = News::orderBy('id','desc')->where('category_id',17)->skip(1)->take(6)->get();

        $expatriates_feature  = News::orderBy('id','desc')->where('category_id',14)->first();
        $expatriates_news = News::orderBy('id','desc')->where('category_id',14)->skip(1)->take(6)->get();

        $islam_feature   = News::orderBy('id','desc')->where('category_id',15)->first();
        $islam_news = News::orderBy('id','desc')->where('category_id',15)->skip(1)->take(4)->get();

        $cartoon_feature   = News::orderBy('id','desc')->where('category_id',19)->first();
        $cartoon_news = News::orderBy('id','desc')->where('category_id',19)->skip(1)->take(4)->get();

        $varied_feature   = News::orderBy('id','desc')->where('category_id',16)->first();
        $varied_news = News::orderBy('id','desc')->where('category_id',16)->skip(1)->take(4)->get();

        return view('frontend.modules.index',compact('cover_feature','beside_cover_feature', 'bellow_cover_feature','national_feature','national_news','politics_feature', 'politics_news','economics_feature','economics_news','internatial_feature','internatial_news','whole_country_feature', 'whole_country_news','sports_feature','sports_news','it_world_feature','it_world_news','capital_feature','capital_news','expatriates_feature','expatriates_news','islam_feature','islam_news','cartoon_feature','cartoon_news','varied_feature','varied_news'));
    }
    public function category(string $slug)
    {
$category = Category::where('slug',$slug)->first();
$sub_categories = SubCategory::where('category_id',$category->id)->get();
$feature_news = News::where('category_id',$category->id)->first();
$beside_feature_news = News::where('category_id',$category->id)->take(2)->skip(1)->get();
$all_news = Category::with('sub_category.news')->findOrFail($category->id);

return view('frontend.modules.category',compact('category','sub_categories','feature_news','beside_feature_news','all_news'));
    }
    public function single_news()
    {

        return view('frontend.modules.singleNews');
    }


//    public function add_slug(): void
//    {
//        $categories = News::all();
//
//        foreach ($categories as $category){
//            $data['slug'] =  Str::slug($category->title, '-');
//            $category->update($data);
//        }
//    }

}
