<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static create(array $data)
 * @method static orderBy(string $string, string $string1)
 * @method static select(string $string, string $string1)
 * @method static pluck(string $string, string $string1)
 * @method static where(string $string, string $slug)
 */
class SubCategory extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(user::class);
    }

    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }
}
