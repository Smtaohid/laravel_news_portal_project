@extends('dashboard.layout.master')
@section('page_title','Category List')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-header"><h3 class="text-center"> Category list</h3></div>

                @if (session('msg'))
                    <div class="alert alert-success">
                        {{session('msg')}}
                    </div>
                @endif

                <div class="card-body">
                    <table class="table table-responsive table-bordered table-striped text-center ">
                           <thead>
                           <tr class="text-center">
                               <th>Sl</th>
                               <th>Category Name</th>
                               <th>Order By</th>
                               <th>Stats</th>
                               <th>Create By</th>
                               <th>Create At</th>
                               <th>Update At</th>
                               <th>Action</th>
                           </tr>
                           </thead>
                        <tbody>
                        @php $sl = 1 ;@endphp
                        @foreach($categories as $category)
                        <tr>
                            <th>{{$sl++}}</th>
                            <th>{{$category->name}}</th>
                            <th>{{$category->order_by}}</th>
                            <th>{!!$category->status == 1 ? '<span class="text-success">Active</span>':'<span class="text-danger">Inactive</span>'!!}</th>
                            <th>{{$category->user->name}}</th>
                            <th>{{$category->created_at->toDayDateTimeString()}}</th>
                            <th>{{$category->updated_at == $category->created_at ? 'Not Updated':$category->updated_at->toDayDateTimeString()}}</th>
                            <th class="d-flex justify-content-between">
                                <a href="{{route('categories.show', $category->id)}}"> <button class="btn btn-success btn-sm text-white"><i class="fas fa-eye"></i></button></a>
                                <a href="{{route('categories.edit',$category->id)}}"><button class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button></a>
                                {!! Form::open(['method'=>'delete','route'=>['categories.destroy',$category->id]]) !!}
                                {!! Form::button('<i class="fas fa-trash"></i>',['type'=>'submit', 'class'=>'btn btn-danger btn-sm ','onclick'=>'return confirm("Age you Sure")']) !!}
                                {!! Form::close() !!}
                            </th>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
