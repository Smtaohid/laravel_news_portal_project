{!! Form::label('name','Category Name',['class'=>'lead ps-2']) !!}
{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Insert Category name']) !!}
{!! Form::label('order_by','Order By',['class'=>'lead ps-2']) !!}
{!! Form::number('order_by',null, ['class'=>'form-control','placeholder'=>'Insert Menu Order']) !!}
{!! Form::label('status','Status',['class'=>'lead ps-2']) !!}
{!! Form::select('status',['1'=>'Active','2'=>'Inactive'],null,['class'=>'form-control','placeholder'=>'Select Option']) !!}
