<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\CategoryModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class CaregoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
   protected $mode=Category::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
