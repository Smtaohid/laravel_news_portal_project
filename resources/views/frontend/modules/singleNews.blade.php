@extends('frontend.layouts.singleMaster')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">প্রচ্ছদ</a></li>
                    <li class="breadcrumb-item"><a href="#">জাতীয়</a></li>
                    <li class="breadcrumb-item"><a href="#">সরকার</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ঘুস খেলে নামাজ হবে না: মন্ত্রিপরিষদ সচিব</li>
                </ol>
            </nav>
            <h2 class="single-news-title">ঘুস খেলে নামাজ হবে না: মন্ত্রিপরিষদ সচিব</h2>
            <div class="row my-3">
                <div class="col-md-6">
                    <p><i class="fas fa-user"></i> যুগান্তর প্রতিবেদন </p>
                    <p><i class="fas fa-clock"></i> ০৪ সেপ্টেম্বর ২০২১, ০৭:৩৭ পিএম  |  অনলাইন সংস্করণ </p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-3  border-end">
                            <h5>3.3k</h5>
                            <p>Shares</p>
                        </div>
                        <div class="col-9">
                            <div class="social-share">
                                <a href=""><i class="fab fa-facebook-square"></i></a>
                                <a href=""><i class="fab fa-instagram-square"></i></a>
                                <a href=""><i class="fab fa-twitter-square"></i></a>
                                <a href=""><i class="fab fa-linkedin"></i></a>
                                <a href=""><i class="fab fa-pinterest-square"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <img src="{{asset('image/news/5.jpg')}}" alt="single News Photo" class="img-thumbnail" />
            <p class="text-center mt-3"><small>মন্ত্রিপরিষদ সচিব খন্দকার আনোয়ারুল ইসলাম। ফাইল ছবি</small></p>
            <hr>
            <div class="single-page-news-content">
                <p>প্রকল্প পরিচালকদের উদ্দেশে মন্ত্রিপরিষদ সচিব খন্দকার আনোয়ারুল ইসলাম বলেছেন, হারাম কিছু খেলে নামাজ হবে না, ঘুস খেলে নামাজ হবে না। প্রতিদিন আয়নার সামনে দাঁড়ান সারা দিন কী করলেন।</p>
                <p>শনিবার রাজধানীর আগারগাঁওয়ের স্থানীয় সরকার প্রকৌশল অধিদপ্তর (এলজিইডি) ভবনে এক মতবিনিময় সভায় তিনি একথা বলেন। সভায় উপস্থিত ছিলেন এলজিইডির ৭৬ জন প্রকল্প পরিচালক। </p>
                <p>স্থানীয় সরকার বিভাগের উদ্যোগে বাস্তবায়নাধীন প্রকল্পসমূহের প্রকল্প ব্যবস্থাপনা ও জাতীয় শুদ্ধাচার কৌশল বাস্তবায়ন বিষয়ক ওই সভায় বিশেষ অতিথির বক্তব্য দেন মন্ত্রিপরিষদ সচিব। </p>
                <p>এ সময় প্রকল্প পরিচালকদের উদ্দেশে তিনি বলেন, আমাদের ধর্মে স্পষ্ট উল্লেখ আছে, হারাম খেলে নামাজ হবে না। শুধু তাই নয়, হারাম টাকায় কেনা কোনো পোশাক যদি অন্য পোশাক স্পর্শ করে তবে নাপাক হয়ে যাবে। প্রতিদিন আয়নার সামনে দাঁড়ান সারা দিন কী কাজ করলেন। নিজেকে প্রশ্ন করুন।</p>
                <p>সব দেশেই সম্পদ সীমিত, তাই সম্পদের সঠিক ব্যবহারের জন্য প্রকল্প পরিচালকদের নির্দেশ দিয়ে খন্দকার আনোয়ারুল ইসলাম বলেন, আপনাদের কারণে স্যারকে (স্থানীয় সরকার, পল্লী উন্নয়ন ও সমবায়মন্ত্রী মো. তাজুল ইসলাম) প্রধানমন্ত্রীর কাছে অপদস্ত হতে হচ্ছে। স্যারতো প্রকল্প বাস্তবায়ন করেন না। প্রকল্প বাস্তবায়ন করেন আপনারা (পরিচালক)। এলজিইডির কাজ নিয়ে অনেক আপত্তি আছে। কাজের মধ্যে অবশ্যই স্বচ্ছতা আনতে হবে। </p>
                <p>প্রকল্প পরিচালকদের উদ্দেশে মন্ত্রিপরিষদ সচিব আরও বলেন, আমি যতদূর জানি এলজিইডি প্রকল্পে আইন-কানুন মেনে প্রকল্প বাস্তবায়ন করা হয়। তারপরও আইনের মধ্যে থেকে অনেক কিছু করা হয়, আমি খবর রাখি। এখন আরও খোঁজ-খবর রাখব। কোনো ত্রুটি ধরা পড়লে রেহাই নেই। আপনারা প্রকৌশলী, আমি বিজ্ঞানের ছাত্র ছিলাম, আমিও হাফ প্রকৌশলী। সরকার আমাদের বেতন অনেক বাড়িয়েছে। তারপরও যদি কেউ চুরি করে ধরা পড়লে সরাসরি অ্যাকশন নেওয়া হবে। </p>
            </div>

            <div class="row my-5">
                <div class="col-2  border-end">
                    <h5>3.3k</h5>
                    <p>Shares</p>
                </div>
                <div class="col-5">
                    <div class="social-share">
                        <a href=""><i class="fab fa-facebook-square"></i></a>
                        <a href=""><i class="fab fa-instagram-square"></i></a>
                        <a href=""><i class="fab fa-twitter-square"></i></a>
                        <a href=""><i class="fab fa-linkedin"></i></a>
                        <a href=""><i class="fab fa-pinterest-square"></i></a>
                    </div>
                </div>
                <div class="col-5">
                    <h5>ইউটিউব চ্যানেলে সাবস্ক্রাইব করুন</h5>
                    <div class="youtube">
                        <script src="https://apis.google.com/js/platform.js"></script>
                        <div class="g-ytsubscribe" data-channelid="UCTj0n8srmEGtbYzkt7TlfQg" data-layout="full" data-count="default"></div>
                    </div>
                </div>
            </div>
            <h3 class="category-page-news-area-headline">আরও খবর</h3>
            <div class="row">
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-single-item">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
