$(document).ready(function (){
    $('#more_menu').on('click', function () {
        $('#mega_menu').slideToggle(500)
        $('#menu').slideToggle(500)
    })
    $('#search_toggle').on('click', function () {
        $('#menu_container').slideToggle()
        $('#search_area').slideToggle()
    })
    $('#search_close').on('click', function () {
        $('#menu_container').slideToggle()
        $('#search_area').slideToggle()
    })


    $('.photo-gallery').slick({
        arrows: true,
    })


    $('#most_read_news_button').on('click', function () {
        $(this).addClass('active')
        $('#latest_news_button').removeClass('active')
        $('#most_read_news').show()
        $('#latest_news').hide()
    })
    $('#latest_news_button').on('click', function () {
        $(this).addClass('active')
        $('#most_read_news_button').removeClass('active')
        $('#most_read_news').hide()
        $('#latest_news').show()
    })
})