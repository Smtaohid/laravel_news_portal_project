@extends('frontend.layouts.master')
@section('main_content')
    <div class="row">
        <div class="col-lg-8">
            <div class="news-container">
                <a href="{{$cover_feature->id}}">
                    <img src="{{asset('image/uploads/news/'.$cover_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h4>{{$cover_feature->title}}</h4>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            @foreach($beside_cover_feature as $news)
            <div class="news-single-item">
                <a href="{{$news->id}}">
                    <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$news->title}}</h5>
                </a>
            </div>
            @endforeach

        </div>
    </div>
    <div class="row">
        @foreach($bellow_cover_feature as $news)
        <div class="col-lg-4 mt-4">
            <div class="news-single-item">
                <a href="{{$news->id}}">
                    <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$news->title}}</h5>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row mt-5">
        <div class="col-lg-6">
            <div class="section-feature-area">
                <a href="{{$national_feature->id}}">
                    <h4>জাতীয়</h4>
                    <img src="{{asset('image/uploads/news/'.$national_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$national_feature->title}}</h5>
                </a>
            </div>
            @foreach($national_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <div class="col-lg-6">
            <div class="section-feature-area">
                <a href="">
                    <h4>রাজনীতি</h4>
                    <img src="{{asset('image/uploads/news/'.$politics_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$politics_feature->title}}</h5>
                </a>
            </div>
            @foreach($politics_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--international area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>আন্তর্জাতিক</h4>
                    <img src="{{asset('image/uploads/news/'.$internatial_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$internatial_feature->title}}</h5>
                </a>
            </div>
            @foreach($internatial_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
        <!--international area ends-->
        <!--sara desh area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>সারাদেশ</h4>
                    <img src="{{asset('image/uploads/news/'.$whole_country_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$whole_country_feature->title}}</h5>
                </a>
            </div>
            @foreach($whole_country_news as  $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--sara desh area ends-->
        <!--sports area starts-->
        <div class="col-lg-12 my-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>খেলা</h4>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-feature-area">
                        <a href="">
                            <img src="{{asset('image/uploads/news/'.$sports_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5> {{$sports_feature->title}}</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        @foreach($sports_news as $news)
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>{{$news->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!--sports area ends-->
        <!--IT area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>আইটি বিশ্ব</h4>
                    <img src="{{asset('image/uploads/news/'.$it_world_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$it_world_feature->title}}</h5>
                </a>
            </div>
            @foreach($it_world_news as $news)

            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--IT area ends-->
        <!--Economics area stars-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>অর্থনীতি</h4>
                    <img src="{{asset('image/uploads/news/'.$economics_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$economics_feature->title}}</h5>
                </a>
            </div>
            @foreach($economics_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--Economics area ends-->
        <!--Capital area stats-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>রাজধানী</h4>
                    <img src="{{asset('image/uploads/news/'.$capital_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$capital_feature->title}}</h5>
                </a>
            </div>
            @foreach($capital_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--Capital area ends-->
        <!--Foreign area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>পরবাস</h4>
                    <img src="{{asset('image/uploads/news/'.$expatriates_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$expatriates_feature->title}}</h5>
                </a>
            </div>
            @foreach($expatriates_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--Foreign area ends-->
        <!--Islam area starts-->
        <div class="col-lg-12 my-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>ইসলাম ও জীবন</h4>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-feature-area">
                        <a href="">
                            <img src="{{asset('image/uploads/news/'.$islam_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>{{$islam_feature->title}}</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        @foreach($islam_news as $news)
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>{{$news->title}}</h5>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!--Islam area ends-->
        <!--Bicchu area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>বিচ্ছু</h4>
                    <img src="{{asset('image/uploads/news/'.$cartoon_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$cartoon_feature->title}}</h5>
                </a>
            </div>
            @foreach($cartoon_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <!--Bicchu area ends-->
        <!--chitro bichitro area starts-->
        <div class="col-lg-6 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>চিত্র বিচিত্র</h4>
                    <img src="{{asset('image/uploads/news/'.$varied_feature->photo)}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>{{$varied_feature->title}}</h5>
                </a>
            </div>
            @foreach($varied_news as $news)
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/uploads/news/'.$news->photo)}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>{{$news->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
             @endforeach
        </div>
        <!--chitro bichitro area ends-->
        <!--Editors area starts-->
        <div class="col-lg-4 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>সম্পাদকীয়</h4>
                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!--Editors area ends-->
        <!--Batayon area stats-->
        <div class="col-lg-4 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>বাতায়ন</h4>
                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!--Batayon area ends-->
        <!--Drishtipat area Stats-->
        <div class="col-lg-4 mt-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>দৃষ্টিপাত</h4>
                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে বিআইডব্লিউটিসি</h5>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="section-single-news">
                <a href="">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                        </div>
                        <div class="col-8">
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!--Drishtipat area ends-->
        <!--Life Style area starts-->
        <div class="col-lg-12 my-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>লাইফ স্টাইল</h4>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-feature-area">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Life Style area ends-->
        <!--Ak din proti din area starts-->
        <div class="col-lg-12 my-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>একদিন প্রতিদিন</h4>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-feature-area">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Ak din proti din area ends-->
        <!--entertainment din area starts-->
        <div class="col-lg-12 my-5">
            <div class="section-feature-area">
                <a href="">
                    <h4>বিনোদন</h4>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-feature-area">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="news-single-item">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--entertainment din area ends-->
        <!--photo Gallery din area starts-->
        <div class="col-lg-12 my-5">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-feature-area">
                        <a href="">
                            <h4>ফটো গ্যালারি</h4>
                        </a>
                    </div>
                    <div class="photo-gallery">
                        <div class="position-relative">
                            <img src="{{asset('image/gallery/1.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                            <div class="slick-overly">
                                <p>কঠোর বিধি নিষেধ তুলে দেওয়ার পর রাজধানীর পুরান ঢাকার সিদ্দিক বাজার এলাকায়
                                    ভয়াবহ যানজট।1</p>
                            </div>
                        </div>
                        <div class="position-relative">
                            <img src="{{asset('image/gallery/2.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                            <div class="slick-overly">
                                <p>কঠোর বিধি নিষেধ তুলে দেওয়ার পর রাজধানীর পুরান ঢাকার সিদ্দিক বাজার এলাকায়
                                    ভয়াবহ যানজট।2</p>
                            </div>
                        </div>
                        <div class="position-relative">
                            <img src="{{asset('image/gallery/3.jpg')}}" class="img-fluid" alt="Feature Photo"/>
                            <div class="slick-overly">
                                <p>কঠোর বিধি নিষেধ তুলে দেওয়ার পর রাজধানীর পুরান ঢাকার সিদ্দিক বাজার এলাকায়
                                    ভয়াবহ যানজট। 3</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="section-feature-area">
                        <a href="">
                            <h4>ভিডিও গ্যালারি</h4>
                        </a>
                    </div>
                    <div class="section-feature-area-gallery">
                        <a href="">
                            <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail" alt="Feature Photo"/>
                            <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                বিআইডব্লিউটিসি</h5>
                        </a>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="section-feature-area-gallery">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="section-feature-area-gallery">
                                <a href="">
                                    <img src="{{asset('image/news/3.jpg')}}" class="img-thumbnail"
                                         alt="Feature Photo"/>
                                    <h5>পদ্মা সেতুর পিলারে বারবার ধাক্কা, ফেরিতে রাবারের আস্তর লাগাচ্ছে
                                        বিআইডব্লিউটিসি</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--photo Gallery din area ends-->
    </div>
@endsection
