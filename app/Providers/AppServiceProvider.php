<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\News;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {

        $latest_news = News::latest()->take(15)->get();
        $most_read_news = News::take(15)->get();
        $main_menu = Category::orderBy('order_by','asc')->take(10)->get();
        $all_menu = Category::orderBy('order_by','asc')->get();
        View::share(['latest_news'=>$latest_news, 'most_read_news'=>$most_read_news,'main_menu'=>$main_menu,'all_menu'=>$all_menu]);


        Paginator::useBootstrap();

    }
}
