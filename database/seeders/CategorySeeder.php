<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<50; $i++){
            $data['name'] = 'Category'.$i;
            $data['status'] = 1 ;
            $data['order_by'] = $i;
            $data['user_id'] = 1;
            Category::create($data);
        }
    }
}
